#include "Renderer.hpp"


#include "Game.hpp"



byte Renderer::getPixel(byte pitch) {
    return g->out.getPixel(pitch);
}

byte Renderer::getPixel(unsigned int x, unsigned int y) {
    return g->out.getPixel(x, y);
}

void Renderer::setPixel(byte pitch, byte color) {
    g->out.setPixel(pitch, color);
}

void Renderer::setPixel(unsigned int x, unsigned int y, byte color) {
    g->out.setPixel(x, y, color);
}



void Renderer::clear() {
    g->out.clear();
}

void Renderer::display() {
    frameCounter++;
    g->out.display();
}



void Renderer::drawHorizontalLine(int x, int y, byte color, int width) {
    for (int i = x; i < x + width; i++) {
        setPixel(i, y, color);
    }
}

void Renderer::drawVerticalLine(int x, int y, byte color, int height) {
    for (int i = y; i < y + height; i++) {
        setPixel(x, i, color);
    }
}

void Renderer::drawRect(int x, int y, int width, int height, byte color) {
    if (x > 9 || y > 9) return;

    for (int i = x; i < x + width; i++) {
        for (int j = y; j < y + height; j++) {
            setPixel(i, j, color);
        }
    }
}



Renderer::Renderer(Game* g) {
    this->g = g;
}
