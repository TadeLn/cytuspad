#include "SongEntry.hpp"


#include <iomanip>
#include "tutils/json/Object.hpp"
#include "tutils/String.hpp"



int SongEntry::getDifficulty(unsigned int index) {
    if (index >= difficulties.size()) {
        return -1;
    } else {
        return difficulties[index];
    }
}

int SongEntry::getDifficultyCount() {
    return difficulties.size();
}



std::shared_ptr<tu::json::Node> SongEntry::toJSON() const {
    auto root = std::make_shared<tu::json::Object>();
    // [TODO]
    return root;
}



bool SongEntry::loadJSON(std::shared_ptr<tu::json::Node> json) {
    auto root = json->cast<tu::json::Object>();

    json->getProperty("title")->load(this->title);
    json->getProperty("artist")->load(this->artist);
    json->getProperty("charter")->load(this->charter);
    json->getProperty("previewOffset")->load(this->previewOffset);
    json->getProperty("previewLength")->load(this->previewLength);
    json->getProperty("playlistOrder")->load(this->playlistOrder);

    auto difficulties = json->getProperty("difficulties")->getOr<tu::json::Node::array>();
    for (int i = 0; i < difficulties.size(); i++) {
        int value = difficulties[i]->getOr(-1);
        this->difficulties.push_back(value);
    }
    
    return true;
}

SongEntry SongEntry::fromJSON(std::shared_ptr<tu::json::Node> json) {
    SongEntry result;
    result.loadJSON(json);
    return result;
}

SongEntry SongEntry::fromFile(std::string filename) {
    if (filename == "") {
        SongEntry entry;
        entry.filename = filename;
        return entry;

    } else {
        SongEntry entry = SongEntry::fromJSON(tu::json::Node::fromFile(
            std::filesystem::path(filename) / "info.json"
        ));
        entry.filename = filename;

        for (int i = 0; i < 8; i++) {
            if (std::filesystem::exists(std::filesystem::path(filename) / (std::string("chart_") + std::to_string(i) + ".json"))) {
                if (entry.getDifficulty(i) == -1) {
                    entry.setDifficulty(i, 0);
                }
            }
        }
        return entry;
    }
}



std::string SongEntry::toString() const {
    return tu::String::str(toString1(), " ", toString2(), " ", toString3());
}

std::string SongEntry::toString1() const {
    return tu::String::str("\"", title, "\" - ", artist);
}

std::string SongEntry::toString2() const {
    return tu::String::str("[", filename, "]");
}

std::string SongEntry::toString3() const {
    return tu::String::str("{"
        "order=", playlistOrder, ", "
        "preview=", previewOffset, "ms+", previewLength, "ms, "
        "diffs=", difficulties, ""
    "}");
}

std::ostream& operator<<(std::ostream& stream, const SongEntry& instance) {
    std::stringstream ss;
    ss << std::setfill(' ') << std::left
        << std::setw(65) << instance.toString1()
        << std::setw(0) << " "
        << std::setw(30) << instance.toString2()
        << std::setw(0) << " "
        << std::setw(0) << instance.toString3();
    stream << ss.str();
    return stream;
}



SongEntry::SongEntry(std::string filename, std::string title, std::string artist, int previewOffset, int previewLength) {
    this->filename = filename;
    this->title = title;
    this->artist = artist;
    this->previewOffset = previewOffset;
    this->previewLength = previewLength;
}



void SongEntry::setDifficulty(unsigned int index, int value) {
    if (value == -1) {
        if (index < difficulties.size()) {
            difficulties[index] = value;
            return;
        }
    } else {
        while (index >= difficulties.size()) {
            difficulties.push_back(-1);
        }
        difficulties[index] = value;
    }
}
