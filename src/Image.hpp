#pragma once


#include <array>
#include <optional>

#include "Renderer.hpp"



class Image {

public:
    std::array<std::array<byte, 8>, 8> pixels;
    void draw(Renderer& r);

    static std::optional<Image> getFromFile(std::string filename);

    Image();
    Image(std::array<std::array<byte, 8>, 8> pixels);
    Image(byte pixels[64]);

    static void reloadImages();

private:
    static std::map<std::string, std::optional<Image>> loadedImages;

};
