#include "ControllerInput.hpp"



ControllerInput::MidiMessage::MidiMessage(double timestamp, std::vector<byte> data) {
    this->timestamp = timestamp;
    this->data = data;
}



void ControllerInput::handleMIDIMessage(double timestamp, std::vector<byte> data) {
    this->unhandledMessages.push(MidiMessage(timestamp, data));
}

void ControllerInput::destroyMIDIDevice() {
    if (midi != nullptr) {
        delete midi;
    }
}

void ControllerInput::setMIDIDevice(RtMidiIn* device) {
    destroyMIDIDevice();
    this->midi = device;
}

RtMidiIn* ControllerInput::getMIDIDevice() {
    return midi;
}

void ControllerInput::setChannel(int channel) {
    this->channel = channel;
}

int ControllerInput::getChannel() {
    return channel;
}



bool ControllerInput::pollMessage(MidiMessage& msg) {
    if (unhandledMessages.empty()) return false;
    msg = unhandledMessages.front();
    unhandledMessages.pop();

    byte messageType = msg.data.at(0);
    byte messageChannel = messageType & 0b00001111;
    byte messageTypeOnly = messageType & 0b11110000;
    if (messageChannel == channel) {
        if (messageTypeOnly == 144 || messageTypeOnly == 176) {
            byte pitch = msg.data.at(1);
            byte velocity = msg.data.at(2);
            this->currentValues[pitch] = velocity;
        }
    }
    return true;
}



bool ControllerInput::isPressed(byte pitch) {
    return getVelocity(pitch) != 0;
}

byte ControllerInput::getVelocity(byte pitch) {
    return this->currentValues[pitch];
}



ControllerInput::ControllerInput() {
    this->midi = nullptr;
}

ControllerInput::ControllerInput(RtMidiIn* device) {
    this->setMIDIDevice(device);
}

ControllerInput::~ControllerInput() {
    this->destroyMIDIDevice();
}
