#pragma once


#include <SFML/Audio.hpp>

#include "ControllerInput.hpp"
#include "ControllerOutput.hpp"
#include "sound/SFXManager.hpp"
#include "sound/MusicManager.hpp"
#include "SongEntry.hpp"
#include "Settings.hpp"

class Game;
class Renderer;
class Screen;



class Game {

public:
    Renderer* r;
    Screen* currentScreen;

    Settings settings;
    void reloadSettings();

    ControllerInput in;
    ControllerOutput out;

    MusicManager music;
    SFXManager sfx;

    std::map<std::string, SongEntry> songs;
    std::vector<std::pair<int, std::string>> sortedSongs;
    void scanSongs(std::filesystem::path path);
    void scanSongs();
    void printSongs();


    void openScreen(Screen* screen, bool clear = true);
    void closeScreen(bool clear = true);
    void replaceScreen(Screen* screen, bool clear = true);

    void start();
    void loop();
    void exit();

    Game();
    ~Game();

    static Game* inst();

private:
    bool isRunning = false;
    static Game* i;


};
