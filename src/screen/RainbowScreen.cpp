#include "RainbowScreen.hpp"

#include "Renderer.hpp"
#include "ColorTestScreen.hpp"
#include "screen/SongSelectScreen.hpp"



void RainbowScreen::draw(Renderer& r) {
    for (int i = 0; i < 100; i++) {
        byte pixel = r.getPixel(i);
        if (pixel != 0) {
            auto coords = Util::pitchToCoords(i);
            pixel = ((coords.x + coords.y + tick) % 127) + 1;
            r.setPixel(i, pixel);
        }
    }
    r.display();

    auto now = std::chrono::steady_clock::now();
    if (now - lastDraw > std::chrono::milliseconds(60)) {
        tick++;
        if (tick >= 127) {
            tick = 0;
        }
        lastDraw = now;
    }
}

void RainbowScreen::loop() {
    if (g->in.isPressed(Pad::SESSION) &&
        g->in.isPressed(Pad::NOTE) &&
        g->in.isPressed(Pad::DEVICE) &&
        g->in.isPressed(Pad::USER)) {
        g->closeScreen();
    }
}

void RainbowScreen::onInput(byte pitch, byte velocity) {
    Renderer* r = g->r;

    if (velocity != 0) {
        if (r->getPixel(pitch) == 0) {
            r->setPixel(pitch, 1);
        } else {
            r->setPixel(pitch, 0);
        }
        r->display();
    }
}
