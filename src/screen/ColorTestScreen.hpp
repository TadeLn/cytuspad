#include "Screen.hpp"



class ColorTestScreen : public Screen {

    void onInput(byte pitch, byte velocity);
    void draw(Renderer& r);

    int colorTestOffset = 0;
};
