#include "GameScreen.hpp"

#include "Renderer.hpp"



void GameScreen::open(Game* g) {
    Screen::open(g);

    screenOpenTime = std::chrono::steady_clock::now();
    introTransition = true;

    std::filesystem::path path = std::filesystem::path() / "res" / "charts" / filename;
    this->chart = Chart::fromFile(path / (std::string("chart_") + std::to_string(diff) + ".json"));

    const int transition = 100;
    auto then = g->music.q_load(path / "audio.ogg", transition)->getTime();
    g->music.q_stop(then);
    g->music.q_setLooping(false, then);
    g->music.q_setVolume(1, then);
    g->music.q_play(then + std::chrono::milliseconds(1500));

    SongEntry songEntry = g->songs[filename];
    std::cout << "Now playing: " << songEntry.toString1() << " [" << Chart::getDiffName(diff) << "] " << songEntry.toString2() << "\n";
    
}




void GameScreen::draw(Renderer& r) {
    currentMs = g->music.getPosition();
    currentTick = this->chart.msToTick(currentMs, globalOffset);

    if (introTransition) {
        renderIntro(r);
    } else {
        render(r);
    }
}



void GameScreen::onInput(byte pitch, byte velocity) {
    if (velocity > 0) {
        if (pitch == Pad::UP) {
            const int transition = 50;
            if (g->music.willBePlaying()) {
                g->music.q_pause(transition);
            } else {
                g->music.q_play(transition);
            }

        } else if (pitch == Pad::LEFT) {
            g->music.setPosition(g->music.getPosition() - 3000);

        } else if (pitch == Pad::RIGHT) {
            g->music.setPosition(g->music.getPosition() + 3000);

        } else if (pitch == Pad::SESSION) {
            g->closeScreen();

        } else {
            if (!g->settings.tapfxFilename.empty()) {
                auto path = std::filesystem::path("res") / "sfx" / "tapfx" / g->settings.tapfxFilename;
                g->sfx.play(path, SFXManager::HIT_SOUND);
            }
        }
    }
}



GameScreen::GameScreen(std::string filename, int diff) {
    this->filename = filename;
    this->diff = diff;
}



// [TODO] change so `active` is a double from 0 to 1 instead of an int from 0 to 2
// [TODO] add custom color palettes / gradients
void GameScreen::drawNote(Renderer& r, long x, long y, Note& note, int pageIndex, int active) {
    Page page = chart.pageList[note.pageIndex];
    Page::ScanLineDirection direction = page.scanLineDirection;

    if (active == 0) {
        return;
    }
    if (x < 1 || x > 8 || y < 1 || y > 8) {
        return;
    }

    // Draw hold rectangles
    if (note.holdTick != 0) {
        Color color;
        bool longHold = note.type == Note::NoteType::HOLD_LONG;
        
        if (active > 1) {
            color = longHold ? Color::HOLD_LONG : Color::HOLD;
        } else {
            color = longHold ? Color::HOLD_LONG_DIM : Color::HOLD_DIM;
        }

        // [TODO] detect if the note is held
        if (note.tick < currentTick) {
            if (currentMs % 100 < 50) {
                color = longHold ? Color::HOLD_LONG_ACTIVE : Color::HOLD_ACTIVE;
            }
        }


        if (note.type == Note::NoteType::HOLD) {
            int length = ((double)note.holdTick / (page.endTick - page.startTick)) * 9;

            if (direction == Page::ScanLineDirection::UP) {
                r.drawRect(x, y, 1, length, color);
            } else {
                r.drawRect(x, y - length, 1, length, color);
            }

        } else if (note.type == Note::NoteType::HOLD_LONG) {
            r.drawRect(x, 0, 1, 10, color);
        }
    }


    // Draw drag note lines [TODO]


    typedef Note::NoteType N;
    typedef Color C;

    // Draw notes
    if (note.type == N::HIT || note.type == N::SWIPE || note.type == N::HIT_DRAG) {
        if (direction == Page::ScanLineDirection::UP) {
            r.setPixel(x, y, (active > 1) ? C::NOTE_HIT_UP : C::NOTE_HIT_UP_DIM);
        } else {
            r.setPixel(x, y, (active > 1) ? C::NOTE_HIT_DOWN : C::NOTE_HIT_DOWN_DIM);
        }

    } else if (note.type == N::HOLD) {
        if (direction == Page::ScanLineDirection::UP) {
            r.setPixel(x, y, (active > 1) ? C::NOTE_HOLD_UP : C::NOTE_HOLD_UP_DIM);
        } else {
            r.setPixel(x, y, (active > 1) ? C::NOTE_HOLD_DOWN : C::NOTE_HOLD_DOWN_DIM);
        }

    } else if (note.type == N::HOLD_LONG) {
        r.setPixel(x, y, (active > 1) ? C::NOTE_HOLD_LONG : C::NOTE_HOLD_LONG_DIM);

    } else if (note.type == N::DRAG || note.type == N::DRAG_TICK || note.type == N::HIT_DRAG_TICK) {
        if (direction == Page::ScanLineDirection::UP) {
            r.setPixel(x, y, (active > 1) ? C::NOTE_DRAG_UP : C::NOTE_DRAG_UP_DIM);
        } else {
            r.setPixel(x, y, (active > 1) ? C::NOTE_DRAG_DOWN : C::NOTE_DRAG_DOWN_DIM);
        }

    } else if (note.type == N::PIANO_HIT) {
        r.setPixel(x, y, (active > 1) ? C::NOTE_HIT_UP : C::NOTE_HIT_UP_DIM);

    } else if (note.type == N::PIANO_DRAG) {
        r.setPixel(x, y, (active > 1) ? C::NOTE_DRAG_UP : C::NOTE_DRAG_UP_DIM);

    } else {
        r.setPixel(x, y, (active > 1) ? C::NOTE_UNKNOWN : C::NOTE_UNKNOWN_DIM);
    }
}

void GameScreen::drawRotatedSquare(Renderer& r, int size, byte color) {
    if (size <= 0) {
        return;
    }

    // Top left corner
    for (int i = 0; i < size; i++) {
        const int originX = 4, originY = 5;

        r.setPixel(originX + i - size + 1, originY + i, color);
    }

    // Top right corner
    for (int i = 0; i < size; i++) {
        const int originX = 5, originY = 5;

        r.setPixel(originX + size - 1 - i, originY + i, color);
    }

    // Bottom left corner
    for (int i = 0; i < size; i++) {
        const int originX = 4, originY = 4;

        r.setPixel(originX + i - size + 1, originY - i, color);
    }

    // Bottom right corner
    for (int i = 0; i < size; i++) {
        const int originX = 5, originY = 4;

        r.setPixel(originX + size - 1 - i, originY - i, color);
    }
}

void GameScreen::drawCenteredLine(Renderer& r, int y, int width, byte color) {
    r.drawHorizontalLine(5 - (width / 2), y, color, width);
}



void GameScreen::renderIntro(Renderer& r) {
    auto now = std::chrono::steady_clock::now();
    auto duration = now - screenOpenTime;
    int animFrame = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count() / 50;

    drawRotatedSquare(r, animFrame, 3);
    drawRotatedSquare(r, animFrame - 1, 2);
    drawRotatedSquare(r, animFrame - 2, 1);
    drawRotatedSquare(r, animFrame - 3, 0);
    
    r.display();

    if (animFrame > 20) {
        introTransition = false;    
    }
}

void GameScreen::render(Renderer& r) {
    r.clear();

    // Draw line
    int currentPageIndex = chart.getPageFromTick(currentTick);
    Page::ScanLineDirection direction = Page::ScanLineDirection::UP;
    if (currentPageIndex != -1) {
        direction = chart.pageList[currentPageIndex].scanLineDirection;
    }

    if (chart.isPlayingPiano(currentTick)) {
        direction = Page::ScanLineDirection::UP;
    }


    // How long does the line grow (in ms)
    const int lineIntroTransition = 1000;
    int lineWidth = ((double)g->music.getPosition() / lineIntroTransition) * 10;
    if (lineWidth < 0) lineWidth = 0;
    if (lineWidth > 10) lineWidth = 10;
    

    double lineY = chart.getY(currentTick, currentTick);
    int linePadY = chart.getPadY(lineY, direction);
    if (direction == Page::ScanLineDirection::UP) {
        drawCenteredLine(r, linePadY - 1, lineWidth, Color::LINE_DIM);
    } else {
        drawCenteredLine(r, linePadY + 1, lineWidth, Color::LINE_DIM);
    }
    drawCenteredLine(r, linePadY, lineWidth, Color::LINE);


    Page currentPage = chart.pageList[currentPageIndex];
    double lineProgress = (double)(currentTick - currentPage.startTick) / (currentPage.endTick - currentPage.startTick);


    // Draw notes
    for (auto it = chart.noteList.rbegin(); it != chart.noteList.rend(); ++it) {
        Note note = *it;

        long x = chart.getNoteX(note);
        long y = chart.getNoteY(note, currentTick);
        int pageIndex = note.effectivePageIndex();

        // Filter out notes that should not be drawn
        // const bool filter = pageIndex > chart.getPageFromTick(currentTick + chart.beatToTick(2)); // More visibility
        // const bool filter = pageIndex > chart.getPageFromTick(currentTick) + 1; // More accurate to the original
        const bool filter = pageIndex > chart.getPageFromTick(currentTick) + 3;
        if (filter) continue;
        if (note.tick + note.holdTick < currentTick) {
            if (showDragNotes) {
                if (note.nextId == 0 || note.nextId == -1) {
                    continue;
                } else {
                    if (chart.noteList[note.nextId].tick < currentTick) {
                        continue;
                    }
                }
            } else {
                continue;
            }
        }


        // Draw notes
        if (chart.isPlayingPiano(note.tick)) {
            drawNote(r, x, y + 1, note, pageIndex, 2);

        } else {
            int active = 0;
            if (groupedMode) {
                Page previousPage = chart.pageList[pageIndex - 1];
                Page page = chart.pageList[pageIndex];
                active += currentTick > previousPage.startTick;
                active += currentTick > page.startTick;

            } else {
                /*
                // ms based calculation
                const double transition = 750;
                const double endWait = 250;
                double noteProgress = (chart.tickToMs(note.tick, globalOffset) - chart.tickToMs(currentTick, globalOffset) - endWait) / transition;

                active = std::max(0.0, std::min(1.0, 1.0 - noteProgress)) * 2;
                */

                // page based calculation
                Page notePage = chart.pageList[pageIndex];
                double noteProgress = (pageIndex - currentPageIndex);
                noteProgress += (double)(note.tick - notePage.startTick) / (notePage.endTick - notePage.startTick);

                // Distance of the note from the line (in pages)
                double distance = noteProgress - lineProgress;
                active += distance < 1;
                active += distance < 0.5;
            }

            drawNote(r, x, y, note, pageIndex, active);
        }
    }

    r.display();
}
