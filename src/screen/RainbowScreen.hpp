#pragma once


#include "Screen.hpp"



class RainbowScreen : public Screen {


public:
    void draw(Renderer& r);
    void loop();
    void onInput(byte pitch, byte velocity);

    std::chrono::steady_clock::time_point lastDraw = std::chrono::steady_clock::now();
    int tick = 0;

};
