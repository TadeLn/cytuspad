#include "Screen.hpp"


#include <vector>



class SongSelectScreen : public Screen {


public:
    void open(Game* g);
    void reopen();
    void onInput(byte pitch, byte velocity);
    void draw(Renderer& r);

    void randomSong();
    void updateSong(bool force = false);
    void updateDifficulty();
    void setDifficulty(unsigned int newDifficulty = -1);

private:
    void drawButtonColor(byte pitch, byte inactiveColor = 3, byte activeColor = 21);

    int selectedSong = 0;
    unsigned int selectedDifficulty = 2;

    SongEntry song;
    int lastSelectedSong = 0;


};
