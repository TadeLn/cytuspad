#pragma once


#include "Screen.hpp"



class ImageEditScreen : public Screen {


public:
    void draw(Renderer& r);
    void onInput(byte pitch, byte velocity);

};
