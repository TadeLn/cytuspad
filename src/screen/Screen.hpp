#pragma once


#include "Util.hpp"
#include "Game.hpp"



class Screen {


public:
    virtual void open(Game* g);
    virtual void reopen();
    virtual void close();

    virtual void draw(Renderer& r);
    virtual void loop();
    virtual void onInput(byte pitch, byte velocity);

    Screen* parentScreen = nullptr;

protected:
    Game* g;

};
