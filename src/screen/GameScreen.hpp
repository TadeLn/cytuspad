#pragma once


#include <SFML/Audio.hpp>

#include "Screen.hpp"
#include "chart/Chart.hpp"



class GameScreen : public Screen {


public:
    void open(Game* g);
    void draw(Renderer& r);
    void onInput(byte pitch, byte velocity);

    bool showDragNotes = false;
    bool groupedMode = false;

    double globalOffset = 70;
    int currentTick = 0;
    int currentMs = 0;
    Chart chart;


    GameScreen(std::string filename = "", int diff = 0);

private:
    std::string filename;
    int diff;

    bool introTransition;
    std::chrono::steady_clock::time_point screenOpenTime;


    void drawNote(Renderer& r, long x, long y, Note& note, int pageIndex, int active);
    void drawRotatedSquare(Renderer& r, int size, byte color);
    void drawCenteredLine(Renderer& r, int y, int width, byte color);

    void render(Renderer& r);
    void renderIntro(Renderer& r);


};
