#include "ColorTestScreen.hpp"


#include "Renderer.hpp"



void ColorTestScreen::onInput(byte pitch, byte velocity) {
    if (velocity != 0) {
        if (pitch == Pad::UP) {
            colorTestOffset += 10;
            std::cout << "Color Test Offset: " << colorTestOffset << "\n";

        } else if (pitch == Pad::DOWN) {
            colorTestOffset -= 10;
            std::cout << "Color Test Offset: " << colorTestOffset << "\n";

        } else if (pitch == Pad::LEFT) {
            colorTestOffset--;
            std::cout << "Color Test Offset: " << colorTestOffset << "\n";

        } else if (pitch == Pad::RIGHT) {
            colorTestOffset++;
            std::cout << "Color Test Offset: " << colorTestOffset << "\n";

        } else if (pitch == Pad::SESSION) {
            g->closeScreen();

        } else {
            int colorNo = (((unsigned int)pitch + colorTestOffset) % 128);
            std::cout << "Color No. " << colorNo << " (0x" << std::hex << colorNo << std::dec << ")\n";
        }
    }
}

void ColorTestScreen::draw(Renderer& r) {
    r.clear();
    for (int i = 0; i < 100; i++) {
        r.setPixel(i, (i + colorTestOffset) % 128);
    }
    r.display();
}
