#include "SongSelectScreen.hpp"


#include <filesystem>

#include "Renderer.hpp"
#include "Image.hpp"
#include "screen/GameScreen.hpp"
#include "screen/ColorTestScreen.hpp"
#include "screen/RainbowScreen.hpp"
#include "screen/ImageEditScreen.hpp"



void SongSelectScreen::open(Game* g) {
    Screen::open(g);
    randomSong();
}

void SongSelectScreen::reopen() {
    updateSong(true);
}

void SongSelectScreen::onInput(byte pitch, byte velocity) {
    if (velocity != 0) {

        auto coords = Util::pitchToCoords(pitch);

        if (pitch == Pad::RECORD_ARM) {
            // Refresh songs
            g->scanSongs();

        } else if (pitch == Pad::TRACK_SELECT) {
            // Print songs
            g->printSongs();

        } else if (pitch == Pad::MUTE) {
            // Reload settings
            g->reloadSettings();
            Image::reloadImages();
            SFXManager::reloadSounds();

        } else if (pitch == Pad::RECORD) {
            // Toggle playback
            const int transition = 100;
            if (g->music.willBePlaying()) {
                g->music.q_pause(transition);
            } else {
                g->music.q_play(transition);
            }

        } else if (pitch == Pad::QUANTISE) {
            // Random Song
            randomSong();

        } else if (pitch == Pad::CLICK) {
            // Preview hitsound
            if (!g->settings.tapfxFilename.empty()) {
                auto path = std::filesystem::path("res") / "sfx" / "tapfx" / g->settings.tapfxFilename;
                g->sfx.play(path, SFXManager::HIT_SOUND);
            }

        } else if (pitch == Pad::UP) {
            // First song
            selectedSong = 0;
            updateSong();

        } else if (pitch == Pad::LEFT) {
            // Previous song
            selectedSong--;
            updateSong();

        } else if (pitch == Pad::RIGHT) {
            // Next song
            selectedSong++;
            updateSong();
            
        } else if (pitch == Pad::NOTE) {
            // Start song
            if (song.getDifficulty(selectedDifficulty) != -1) {
                auto path = std::filesystem::path("res") / "sfx" / "gui" / "start_song.ogg";
                g->sfx.play(path, SFXManager::START_SONG_SFX);

                g->openScreen(new GameScreen(song.filename, selectedDifficulty), false);
            }
            
        } else if (pitch == Pad::DEVICE) {
            // Open color test screen
            g->openScreen(new ColorTestScreen());

        } else if (pitch == Pad::USER) {
            // Open rainbow screen
            g->openScreen(new RainbowScreen());

        } else if (coords.x == 9) {
            // Set difficulty
            unsigned int newDiff = 8 - coords.y;
            if (newDiff > 7) {
                newDiff = 7;
            }

            setDifficulty(newDiff);
        }
    }
}

void SongSelectScreen::draw(Renderer& r) {
    r.clear();

    if (g->in.isPressed(Pad::SHIFT)) {
        auto path = std::filesystem::path("res") / "images" / "difficulty";
        const std::array<std::string, 17> images {
            "unknown.bin",
            "1.bin",
            "2.bin",
            "3.bin",
            "4.bin",
            "5.bin",
            "6.bin",
            "7.bin",
            "8.bin",
            "9.bin",
            "10.bin",
            "11.bin",
            "12.bin",
            "13.bin",
            "14.bin",
            "15.bin",
            "15+.bin"
        };

        int diffLevel = song.getDifficulty(selectedDifficulty);
        if (diffLevel < 0) {
            diffLevel = 0;
        }
        if (diffLevel >= images.size()) {
            diffLevel = images.size() - 1;
        }

        Image image = Image::getFromFile(path / images[diffLevel]).value_or(Image());
        image.draw(r);

    } else {
        auto path = std::filesystem::path("res") / "charts" / song.filename / "background.bin";
        auto img = Image::getFromFile(path);
        if (img.has_value()) {
            Image image = img.value();
            image.draw(r);
        } else {
            auto path = std::filesystem::path("res") / "images" / "background" / "default.bin";
            Image image = Image::getFromFile(path).value_or(Image());
            image.draw(r);
        }
    }


    // Bottom buttons
    drawButtonColor(Pad::RECORD_ARM);
    drawButtonColor(Pad::TRACK_SELECT);
    drawButtonColor(Pad::MUTE);

    // Left buttons
    if (g->music.willBePlaying()) {
        r.setPixel(Pad::RECORD, 21);
    } else {
        r.setPixel(Pad::RECORD, 5);
    }
    drawButtonColor(Pad::QUANTISE);
    drawButtonColor(Pad::CLICK);
    drawButtonColor(Pad::SHIFT);

    
    const std::array<byte, 8> diffColors{45, 5, 53, 29, 97, 1, 1, 1};
    for (int i = 0; i < 8; i++) {

        if (song.getDifficulty(i) != -1) {
            byte pitch = ((8 - i) * 10) + 9;

            if (i == selectedDifficulty) {
                r.setPixel(pitch, diffColors[i]);
            } else {
                r.setPixel(pitch, 3);
            }
        }
    }


    // Top buttons
    drawButtonColor(Pad::UP);
    drawButtonColor(Pad::LEFT);
    drawButtonColor(Pad::RIGHT);
    r.setPixel(Pad::NOTE, 41);
    r.setPixel(Pad::DEVICE, 53);
    r.setPixel(Pad::USER, 49);

    r.display();
}



void SongSelectScreen::randomSong() {
    selectedSong = rand() % g->sortedSongs.size();
    updateSong();
}

void SongSelectScreen::updateSong(bool force) {
    if (selectedSong == lastSelectedSong && !force) return;
    lastSelectedSong = selectedSong;

    const int transition = 200;
    const int loopTransition = 2000;

    if (selectedSong < 0) {
        selectedSong = g->sortedSongs.size() - 1;
    }
    if (selectedSong >= g->sortedSongs.size()) {
        selectedSong = 0;
    }

    song = g->songs[g->sortedSongs[selectedSong].second];
    std::cout << "Selected song: " << song.toString1() << "\n";

    updateDifficulty();

    std::filesystem::path path = std::filesystem::path() / "res" / "charts" / song.filename / "audio.ogg";
    auto then = g->music.q_load(path, transition)->getTime();
    g->music.q_setPosition(song.previewOffset, then);
    g->music.q_setLooping(true, then);
    g->music.q_setLoopPoints(song.previewOffset, song.previewLength, then);
    g->music.q_setLoopFade(loopTransition, then);
}

void SongSelectScreen::updateDifficulty() {
    while (song.getDifficulty(selectedDifficulty) == -1 && selectedDifficulty > 0) {
        selectedDifficulty--;
    }
}

void SongSelectScreen::setDifficulty(unsigned int newDifficulty) {
    if (song.getDifficulty(newDifficulty) != -1) {
        selectedDifficulty = newDifficulty;
    }
}



void SongSelectScreen::drawButtonColor(byte pitch, byte inactiveColor, byte activeColor) {
    g->r->setPixel(pitch, g->in.isPressed(pitch) ? activeColor : inactiveColor);
}
