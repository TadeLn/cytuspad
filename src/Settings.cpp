#include "Settings.hpp"


#include "tutils/json/Object.hpp"



std::shared_ptr<tu::json::Node> Settings::toJSON() const {
    auto root = std::make_shared<tu::json::Object>();
    // [TODO]
    return root;
}



bool Settings::loadJSON(std::shared_ptr<tu::json::Node> json) {
    auto root = json->cast<tu::json::Object>();

    json->getProperty("tapfxFilename")->load(this->tapfxFilename);
    json->getProperty("verboseInput")->load(this->verboseInput);
    json->getProperty("midiInputChannel")->load(this->midiInputChannel);
    json->getProperty("midiOutputChannel")->load(this->midiOutputChannel);
    
    return true;
}

Settings Settings::fromJSON(std::shared_ptr<tu::json::Node> json) {
    Settings result;
    result.loadJSON(json);
    return result;
}
