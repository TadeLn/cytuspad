#include <iostream>
#include <sstream>
#include <cstdlib>
#include <string.h>
#include <thread>
#include <csignal>


#include "tutils/Exception.hpp"

#include "Game.hpp"



void handleSignal(int signum) {
    delete Game::inst();
    exit(EXIT_SUCCESS);
}


int main() {
    Game* game = new Game();
    signal(SIGINT, handleSignal);
    srand(time(0));

    try {
        game->start();
    } catch (tu::Exception& e) {
        e.print();
        return e.getCode();
    }
    delete game;
    return 0;
}
