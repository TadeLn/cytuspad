#pragma once



enum Color {
    LINE = 3,
    LINE_DIM = 1,


    NOTE_ACTIVE = 3,

    NOTE_UNKNOWN = 72,
    NOTE_UNKNOWN_DIM = 72,

    NOTE_HIT_UP = 37,
    NOTE_HIT_UP_DIM = 39,
    NOTE_HIT_DOWN = 45,
    NOTE_HIT_DOWN_DIM = 47,

    NOTE_HOLD_UP = 57,
    NOTE_HOLD_UP_DIM = 59,
    NOTE_HOLD_DOWN = 84,
    NOTE_HOLD_DOWN_DIM = 83,
    NOTE_HOLD_LONG = 13,
    NOTE_HOLD_LONG_DIM = 15,

    NOTE_DRAG_UP = 49,
    NOTE_DRAG_UP_DIM = 51,
    NOTE_DRAG_DOWN = 53,
    NOTE_DRAG_DOWN_DIM = 55,


    HOLD = 2,
    HOLD_DIM = 1,
    HOLD_ACTIVE = 3,

    HOLD_LONG = HOLD,
    HOLD_LONG_DIM = HOLD_DIM,
    HOLD_LONG_ACTIVE = 12
};
