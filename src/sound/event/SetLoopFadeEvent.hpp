#pragma once


#include "MusicEvent.hpp"



class SetLoopFadeEvent : public MusicEvent {


public:
    int getFade() const;
    
    SetLoopFadeEvent(TimePoint time, int fade);

    std::string toString() const;

private:
    int fade;


};