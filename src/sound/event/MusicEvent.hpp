#pragma once


#include <chrono>
#include <string>

#include "tutils/Stringable.hpp"
#include "tutils/String.hpp"



class MusicEvent : public tu::Stringable {


public:
    typedef std::chrono::steady_clock::time_point TimePoint;
    typedef std::chrono::steady_clock::duration Duration;

    enum Type {
        PLAY,
        STOP,
        PAUSE,
        SET_VOLUME,
        CHANGE_FILE,
        SET_POSITION,
        SET_LOOPING,
        SET_LOOP_POINTS,
        SET_LOOP_FADE
    };

    unsigned int getId() const;
    Type getType() const;
    TimePoint getTime() const;
    Duration getLength() const;

    virtual std::string toString() const;


protected:
    MusicEvent(Type type, TimePoint time, Duration duration);
    TimePoint time;
    Duration duration;

private:
    unsigned int id;
    Type type;

    static unsigned int nextId;


};
