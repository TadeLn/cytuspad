#include "PauseEvent.hpp"



PauseEvent::PauseEvent(TimePoint time)
    : MusicEvent(MusicEvent::PAUSE, time, Duration::zero()) {}

std::string PauseEvent::toString() const {
    return tu::String::str("<PauseEvent #", this->getId(),
        ": time=", std::chrono::duration_cast<std::chrono::milliseconds>(this->time.time_since_epoch()).count(),
        ", dur=", std::chrono::duration_cast<std::chrono::milliseconds>(this->duration).count(),
        ">");
}
