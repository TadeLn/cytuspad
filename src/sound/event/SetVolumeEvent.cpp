#include "SetVolumeEvent.hpp"



double SetVolumeEvent::getFrom() const {
    return from;
}

double SetVolumeEvent::getTo() const {
    return to;
}

SetVolumeEvent::SetVolumeEvent(TimePoint time, Duration duration, double from, double to)
    : MusicEvent(MusicEvent::SET_VOLUME, time, duration) {
    this->from = from;
    this->to = to;
}

std::string SetVolumeEvent::toString() const {
    return tu::String::str("<SetVolumeEvent #", this->getId(),
        ": time=", std::chrono::duration_cast<std::chrono::milliseconds>(this->time.time_since_epoch()).count(),
        ", dur=", std::chrono::duration_cast<std::chrono::milliseconds>(this->duration).count(),
        ", from=", this->from,
        ", to=", this->to,
        ">");
}
