#pragma once


#include "MusicEvent.hpp"



class PlayEvent : public MusicEvent {


public:
    PlayEvent(TimePoint time);

    std::string toString() const;


};
