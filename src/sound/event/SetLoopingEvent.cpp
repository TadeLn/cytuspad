#include "SetLoopingEvent.hpp"



bool SetLoopingEvent::isLooping() const {
    return looping;
}

SetLoopingEvent::SetLoopingEvent(TimePoint time, bool looping)
    : MusicEvent(MusicEvent::SET_LOOPING, time, Duration::zero()) {
    this->looping = looping;
}

std::string SetLoopingEvent::toString() const {
    return tu::String::str("<SetLoopingEvent #", this->getId(),
        ": time=", std::chrono::duration_cast<std::chrono::milliseconds>(this->time.time_since_epoch()).count(),
        ", looping=", looping,
        ">");
}
