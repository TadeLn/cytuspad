#include "ChangeFileEvent.hpp"



std::string ChangeFileEvent::getFilename() const {
    return filename;
}

ChangeFileEvent::ChangeFileEvent(TimePoint time, std::string filename)
    : MusicEvent(MusicEvent::CHANGE_FILE, time, Duration::zero()) {
    this->filename = filename;
}

std::string ChangeFileEvent::toString() const {
    return tu::String::str("<ChangeFileEvent #", this->getId(),
        ": time=", std::chrono::duration_cast<std::chrono::milliseconds>(this->time.time_since_epoch()).count(),
        ", filename=", filename,
        ">");
}
