#pragma once


#include "MusicEvent.hpp"



class StopEvent : public MusicEvent {


public:
    StopEvent(TimePoint time);

    std::string toString() const;


};
