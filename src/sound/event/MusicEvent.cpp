#include "MusicEvent.hpp"



unsigned int MusicEvent::getId() const {
    return id;
}

MusicEvent::Type MusicEvent::getType() const {
    return type;
}

MusicEvent::TimePoint MusicEvent::getTime() const {
    return time;
}

MusicEvent::Duration MusicEvent::getLength() const {
    return duration;
}



std::string MusicEvent::toString() const {
    return tu::String::str("<MusicEvent #", this->id,
        ": type=", this->type,
        ", time=", std::chrono::duration_cast<std::chrono::milliseconds>(this->time.time_since_epoch()).count(),
        ", dur=", std::chrono::duration_cast<std::chrono::milliseconds>(this->duration).count(),
        ">");
}




MusicEvent::MusicEvent(Type type, TimePoint time, Duration duration) {
    this->id = nextId++;
    this->type = type;
    this->time = time;
    this->duration = duration;
}

unsigned int MusicEvent::nextId = 0;
