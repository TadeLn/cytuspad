#pragma once


#include "MusicEvent.hpp"



class SetLoopingEvent : public MusicEvent {


public:
    bool isLooping() const;
    
    SetLoopingEvent(TimePoint time, bool looping);

    std::string toString() const;

private:
    bool looping;


};