#pragma once


#include "MusicEvent.hpp"



class ChangeFileEvent : public MusicEvent {


public:
    std::string getFilename() const;
    
    ChangeFileEvent(TimePoint time, std::string filename);

    std::string toString() const;

private:
    std::string filename;


};