#include "SetLoopFadeEvent.hpp"



int SetLoopFadeEvent::getFade() const {
    return fade;
}

SetLoopFadeEvent::SetLoopFadeEvent(TimePoint time, int fade)
    : MusicEvent(MusicEvent::SET_LOOP_FADE, time, Duration::zero()) {
    this->fade = fade;
}

std::string SetLoopFadeEvent::toString() const {
    return tu::String::str("<SetLoopFadeEvent #", this->getId(),
        ": time=", std::chrono::duration_cast<std::chrono::milliseconds>(this->time.time_since_epoch()).count(),
        ", fade=", fade,
        ">");
}
