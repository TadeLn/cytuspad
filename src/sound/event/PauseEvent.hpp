#pragma once


#include "MusicEvent.hpp"



class PauseEvent : public MusicEvent {


public:
    PauseEvent(TimePoint time);

    std::string toString() const;


};
