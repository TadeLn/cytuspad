#pragma once


#include "MusicEvent.hpp"



class SetPositionEvent : public MusicEvent {


public:
    int getPosition() const;
    
    SetPositionEvent(TimePoint time, int position);

    std::string toString() const;

private:
    int position;


};