#pragma once


#include "MusicEvent.hpp"



class SetLoopPointsEvent : public MusicEvent {


public:
    int getOffset() const;
    int getLength() const;
    
    SetLoopPointsEvent(TimePoint time, int offset, int length);

    std::string toString() const;

private:
    int offset;
    int length;


};