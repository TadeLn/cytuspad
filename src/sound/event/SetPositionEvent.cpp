#include "SetPositionEvent.hpp"



int SetPositionEvent::getPosition() const {
    return position;
}

SetPositionEvent::SetPositionEvent(TimePoint time, int position)
    : MusicEvent(MusicEvent::SET_POSITION, time, Duration::zero()) {
    this->position = position;
}

std::string SetPositionEvent::toString() const {
    return tu::String::str("<SetPositionEvent #", this->getId(),
        ": time=", std::chrono::duration_cast<std::chrono::milliseconds>(this->time.time_since_epoch()).count(),
        ", position=", position,
        ">");
}
