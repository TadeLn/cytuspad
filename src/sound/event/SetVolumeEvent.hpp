#pragma once


#include "MusicEvent.hpp"



class SetVolumeEvent : public MusicEvent {


public:
    double getFrom() const;
    double getTo() const;

    SetVolumeEvent(TimePoint time, Duration duration, double from, double to);

    std::string toString() const;

private:
    double from;
    double to;


};