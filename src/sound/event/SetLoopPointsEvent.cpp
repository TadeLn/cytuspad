#include "SetLoopPointsEvent.hpp"



int SetLoopPointsEvent::getOffset() const {
    return offset;
}

int SetLoopPointsEvent::getLength() const {
    return length;
}

SetLoopPointsEvent::SetLoopPointsEvent(TimePoint time, int offset, int length)
    : MusicEvent(MusicEvent::SET_LOOP_POINTS, time, Duration::zero()) {
    this->offset = offset;
    this->length = length;
}

std::string SetLoopPointsEvent::toString() const {
    return tu::String::str("<SetLoopPointsEvent #", this->getId(),
        ": time=", std::chrono::duration_cast<std::chrono::milliseconds>(this->time.time_since_epoch()).count(),
        ", offset=", offset,
        ", length=", length,
        ">");
}
