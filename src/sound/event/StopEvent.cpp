#include "StopEvent.hpp"



StopEvent::StopEvent(TimePoint time)
    : MusicEvent(MusicEvent::STOP, time, Duration::zero()) {}

std::string StopEvent::toString() const {
    return tu::String::str("<StopEvent #", this->getId(),
        ": time=", std::chrono::duration_cast<std::chrono::milliseconds>(this->time.time_since_epoch()).count(),
        ", dur=", std::chrono::duration_cast<std::chrono::milliseconds>(this->duration).count(),
        ">");
}
