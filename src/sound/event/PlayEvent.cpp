#include "PlayEvent.hpp"



PlayEvent::PlayEvent(TimePoint time)
    : MusicEvent(MusicEvent::PLAY, time, Duration::zero()) {}

std::string PlayEvent::toString() const {
    return tu::String::str("<PlayEvent #", this->getId(),
        ": time=", std::chrono::duration_cast<std::chrono::milliseconds>(this->time.time_since_epoch()).count(),
        ", dur=", std::chrono::duration_cast<std::chrono::milliseconds>(this->duration).count(),
        ">");
}
