#pragma once


#include <string>
#include <vector>
#include <chrono>
#include <memory>

#include <SFML/Audio.hpp>

#include "event/MusicEvent.hpp"



class MusicManager {


public:
    typedef std::chrono::steady_clock Clock;
    typedef std::chrono::steady_clock::time_point TimePoint;

    bool verbose = false;

    void init();
    
    void load(std::string filename);
    void play();
    void pause();
    void stop();
    void setVolume(double volume);
    void setPosition(int position);
    void setLooping(bool loop);
    void setLoopPoints(int offset, int length);
    void setLoopFade(int fade);



    // Queues a "load file" event
    std::shared_ptr<MusicEvent>& q_load(std::string filename, TimePoint now = Clock::now());

    // Queues a "play" event
    std::shared_ptr<MusicEvent>& q_play(TimePoint now = Clock::now());

    // Queues a "pause" event
    std::shared_ptr<MusicEvent>& q_pause(TimePoint now = Clock::now());

    // Queues a "stop" event
    std::shared_ptr<MusicEvent>& q_stop(TimePoint now = Clock::now());

    // Queues a "set volume" event
    std::shared_ptr<MusicEvent>& q_setVolume(double volume, TimePoint now = Clock::now());

    // Queues a "set position" event
    std::shared_ptr<MusicEvent>& q_setPosition(int position, TimePoint now = Clock::now());

    // Queues a "set looping" event
    std::shared_ptr<MusicEvent>& q_setLooping(bool looping, TimePoint now = Clock::now());

    // Queues a "set loop points" event
    std::shared_ptr<MusicEvent>& q_setLoopPoints(int offset, int length, TimePoint now = Clock::now());

    // Queues a "set loop fade" event
    std::shared_ptr<MusicEvent>& q_setLoopFade(int fade, TimePoint now = Clock::now());



    // Stops, loads, and plays the new music, fading the volume
    std::shared_ptr<MusicEvent>& q_load(std::string filename, int ms, TimePoint now = Clock::now());
    
    // Plays the music and gradually increases the volume
    std::shared_ptr<MusicEvent>& q_play(int ms, TimePoint now = Clock::now());

    // Gradually decreases the volume and pauses the music
    std::shared_ptr<MusicEvent>& q_pause(int ms, TimePoint now = Clock::now());

    // Gradually decreases the volume and stops the music
    std::shared_ptr<MusicEvent>& q_stop(int ms, TimePoint now = Clock::now());

    // Gradually changes the volume
    std::shared_ptr<MusicEvent>& q_setVolume(int ms, double from, double to, TimePoint now = Clock::now());



    bool isPlaying();
    bool willBePlaying();
    int getPosition();
    double getVolume();

    void update();

    sf::Music music;
    double masterVolume = 1.0;
    int fade = 0;

private:
    double calculatedVolume = 1.0;
    bool firstLoop = true;
    bool playing = false;

    void queueEvent(std::shared_ptr<MusicEvent> event);
    std::vector<std::shared_ptr<MusicEvent>> events;


};
