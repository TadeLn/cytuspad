#pragma once


#include <string>
#include <map>
#include <memory>
#include <vector>

#include <SFML/Audio.hpp>



// [TODO] make everything not static

class SFXManager {


public:
    enum Channel {
        DEFAULT,
        GUI_SOUND,
        HIT_SOUND,
        START_SONG_SFX
    };
    
    static void init();

    static std::shared_ptr<sf::SoundBuffer> load(std::string filename, std::string id = "");
    static std::shared_ptr<sf::SoundBuffer> loadOr(std::string filename, std::string id = "");
    static std::shared_ptr<sf::SoundBuffer> getBuffer(std::string soundId);

    static void cleanup();
    static void play(std::string filename);
    static void play(std::string filename, Channel channel);

    static void reloadSounds();


private:
    static std::vector<std::shared_ptr<sf::Sound>> activeSounds;
    static std::map<Channel, std::shared_ptr<sf::Sound>> activeSoundChannels;
    static std::map<std::string, std::shared_ptr<sf::SoundBuffer>> loadedSoundData;



};
