#include "SFXManager.hpp"


#include <SFML/Audio.hpp>

#include "tutils/Log.hpp"
#include "tutils/Exception.hpp"
#include "tutils/json/Object.hpp"



void SFXManager::init() {
    auto json = tu::json::Node::fromFile("res/sfx/index.json");
    if (json->getType() == tu::json::Node::OBJECT) {
        auto root = json->cast<tu::json::Object>();
        auto sounds = root->getProperty("sounds")->getOr<tu::json::Node::dictionary>();

        for (auto sound : sounds) {
            try {
                load(sound.second->getOr<std::string>(), sound.first);
            } catch (tu::Exception& e) {
                e.print("Exception while loading sound");
            }
        }
    }
}

std::shared_ptr<sf::SoundBuffer> SFXManager::load(std::string filename, std::string id) {
    tu::Log::log("SFXManager: Loading sound \"", id, "\" (", filename, ")");

    std::shared_ptr<sf::SoundBuffer> buffer = std::make_shared<sf::SoundBuffer>();
    if (!buffer->loadFromFile(filename)) {
        throw tu::Exception("Sound load error", EXCTX);
    }
    
    if (id.empty()) {
        tu::Log::warn("SFXManager: id not given, loading sound with filename (", filename, "), as the id");
        loadedSoundData[filename] = buffer;
    } else {
        loadedSoundData[id] = buffer;
    }
    return buffer;
}

std::shared_ptr<sf::SoundBuffer> SFXManager::loadOr(std::string filename, std::string id) {
    try {
        return load(filename, id);
    } catch (tu::Exception& e) {
        return std::make_shared<sf::SoundBuffer>();
    }
}

std::shared_ptr<sf::SoundBuffer> SFXManager::getBuffer(std::string soundId) {
    auto it = loadedSoundData.find(soundId);
    if (it == loadedSoundData.end()) {
        return loadOr(soundId);
    } else {
        return it->second;
    }
}



void SFXManager::cleanup() {
    for (int i = 0; i < activeSounds.size(); i++) {
        auto it = activeSounds.begin() + i; 
        if ((*it)->getStatus() == sf::SoundSource::Status::Stopped) {
            activeSounds.erase(it);
            i--;
        }
    }
}



void SFXManager::play(std::string soundId) {
    cleanup();

    std::shared_ptr<sf::SoundBuffer> buffer = getBuffer(soundId);

    auto sound = std::make_shared<sf::Sound>(*buffer);
    sound->setVolume(50);
    sound->setPlayingOffset(sf::Time::Zero);
    sound->play();
    activeSounds.push_back(sound);
}

void SFXManager::play(std::string soundId, Channel channelId) {
    std::shared_ptr<sf::SoundBuffer> buffer = getBuffer(soundId);

    std::shared_ptr<sf::Sound> sound;
    auto it2 = activeSoundChannels.find(channelId);
    if (it2 == activeSoundChannels.end()) {
        sound = std::make_shared<sf::Sound>(*buffer);
        sound->setVolume(50);
    } else {
        sound = it2->second;
        sound->stop();
        sound->setBuffer(*buffer);
    }

    sound->setPlayingOffset(sf::Time::Zero);
    sound->play();
    activeSoundChannels[channelId] = sound;
}



void SFXManager::reloadSounds() {
    loadedSoundData.clear();
}



std::vector<std::shared_ptr<sf::Sound>> SFXManager::activeSounds;
std::map<SFXManager::Channel, std::shared_ptr<sf::Sound>> SFXManager::activeSoundChannels;
std::map<std::string, std::shared_ptr<sf::SoundBuffer>> SFXManager::loadedSoundData;
