#include "MusicManager.hpp"


#include <iostream>

#include "event/SetVolumeEvent.hpp"
#include "event/StopEvent.hpp"
#include "event/ChangeFileEvent.hpp"
#include "event/PlayEvent.hpp"
#include "event/PauseEvent.hpp"
#include "event/SetPositionEvent.hpp"
#include "event/SetLoopingEvent.hpp"
#include "event/SetLoopPointsEvent.hpp"
#include "event/SetLoopFadeEvent.hpp"



void MusicManager::init() {

}

void MusicManager::load(std::string filename) {
    music.openFromFile(filename);
    music.pause();
    playing = false;
    firstLoop = true;
}

void MusicManager::play() {
    music.play();
    playing = true;
    firstLoop = true;
}

void MusicManager::pause() {
    music.pause();
    playing = false;
    firstLoop = true;
}

void MusicManager::stop() {
    music.stop();
    playing = false;
    firstLoop = true;
}

void MusicManager::setVolume(double volume) {
    calculatedVolume = volume;
}

void MusicManager::setPosition(int position) {
    if (position < 0) {
        position = 0;
    }
    music.setPlayingOffset(sf::milliseconds(position));
    firstLoop = true;
}

void MusicManager::setLooping(bool looping) {
    music.setLoop(looping);
    firstLoop = true;
}

void MusicManager::setLoopPoints(int offset, int length) {
    music.setLoopPoints(sf::Music::TimeSpan(sf::milliseconds(offset), sf::milliseconds(length)));
    firstLoop = true;
}

void MusicManager::setLoopFade(int fade) {
    this->fade = fade;
    firstLoop = true;
}



std::shared_ptr<MusicEvent>& MusicManager::q_load(std::string filename, TimePoint now) {
    queueEvent(std::make_shared<ChangeFileEvent>(now, filename));
    return events.back();
}

std::shared_ptr<MusicEvent>& MusicManager::q_play(TimePoint now) {
    playing = true;
    queueEvent(std::make_shared<PlayEvent>(now));
    return events.back();
}

std::shared_ptr<MusicEvent>& MusicManager::q_pause(TimePoint now) {
    playing = false;
    queueEvent(std::make_shared<PauseEvent>(now));
    return events.back();
}

std::shared_ptr<MusicEvent>& MusicManager::q_stop(TimePoint now) {
    playing = false;
    queueEvent(std::make_shared<StopEvent>(now));
    return events.back();
}

std::shared_ptr<MusicEvent>& MusicManager::q_setVolume(double volume, TimePoint now) {
    queueEvent(std::make_shared<SetVolumeEvent>(now, MusicEvent::Duration::zero(), volume, volume));
    return events.back();
}

std::shared_ptr<MusicEvent>& MusicManager::q_setPosition(int position, TimePoint now) {
    queueEvent(std::make_shared<SetPositionEvent>(now, position));
    return events.back();
}

std::shared_ptr<MusicEvent>& MusicManager::q_setLooping(bool looping, TimePoint now) {
    queueEvent(std::make_shared<SetLoopingEvent>(now, looping));
    return events.back();
}

std::shared_ptr<MusicEvent>& MusicManager::q_setLoopPoints(int offset, int length, TimePoint now) {
    queueEvent(std::make_shared<SetLoopPointsEvent>(now, offset, length));
    return events.back();
}

std::shared_ptr<MusicEvent>& MusicManager::q_setLoopFade(int fade, TimePoint now) {
    queueEvent(std::make_shared<SetLoopFadeEvent>(now, fade));
    return events.back();
}



std::shared_ptr<MusicEvent>& MusicManager::q_load(std::string filename, int ms, TimePoint now) {
    playing = true;
    auto duration = std::chrono::milliseconds(ms);
    auto then = now + duration;
    queueEvent(std::make_shared<SetVolumeEvent>(now, duration, getVolume(), 0));
    queueEvent(std::make_shared<StopEvent>(then));
    queueEvent(std::make_shared<ChangeFileEvent>(then, filename));
    queueEvent(std::make_shared<PlayEvent>(then));
    queueEvent(std::make_shared<SetVolumeEvent>(then, duration, 0, 1));
    return events.back();
}



std::shared_ptr<MusicEvent>& MusicManager::q_play(int ms, TimePoint now) {
    playing = true;
    auto duration = std::chrono::milliseconds(ms);
    queueEvent(std::make_shared<PlayEvent>(now));
    queueEvent(std::make_shared<SetVolumeEvent>(now, duration, 0, 1));
    return events.back();
}

std::shared_ptr<MusicEvent>& MusicManager::q_pause(int ms, TimePoint now) {
    playing = false;
    auto duration = std::chrono::milliseconds(ms);
    auto then = now + duration;
    queueEvent(std::make_shared<SetVolumeEvent>(now, duration, getVolume(), 0));
    queueEvent(std::make_shared<PauseEvent>(then));
    return events.back();
}

std::shared_ptr<MusicEvent>& MusicManager::q_stop(int ms, TimePoint now) {
    playing = false;
    auto duration = std::chrono::milliseconds(ms);
    auto then = now + duration;
    queueEvent(std::make_shared<SetVolumeEvent>(now, duration, getVolume(), 0));
    queueEvent(std::make_shared<StopEvent>(then));
    return events.back();
}



bool MusicManager::isPlaying() {
    return music.getStatus() == sf::Music::Playing;
}

bool MusicManager::willBePlaying() {
    return playing;
}

int MusicManager::getPosition() {
    return music.getPlayingOffset().asMilliseconds();
}

double MusicManager::getVolume() {
    return calculatedVolume;
}



void MusicManager::update() {
    auto now = std::chrono::steady_clock::now();

    for (auto it = events.begin(); it != events.end(); ++it) {
        std::shared_ptr<MusicEvent> event = *it;
        if (now < event->getTime()) {
            break;
        }

        if (now >= event->getTime() && now <= event->getTime() + event->getLength()) {
            // Update event
            // NOTE: event.duration could be 0
            double progress;
            if (event->getLength() == std::chrono::milliseconds(0)) {
                progress = 1;
            } else {
                long msNow = std::chrono::duration_cast<std::chrono::microseconds>(now - event->getTime()).count();
                long msFinish = std::chrono::duration_cast<std::chrono::microseconds>(event->getLength()).count();

                progress = (double)msNow / (double)msFinish;
            }

            if (event->getType() == MusicEvent::SET_VOLUME) {
                auto volEvent = std::static_pointer_cast<SetVolumeEvent>(event);
                calculatedVolume = volEvent->getFrom() + ((volEvent->getTo() - volEvent->getFrom()) * progress);
            }

            if (verbose) {
                // std::cout << "Update event: " << event->toString() << "\n";
            }
        }

        if (now > event->getTime() + event->getLength()) {
            // Finish event
            if (event->getType() == MusicEvent::PLAY) {
                play();

            } else if (event->getType() == MusicEvent::STOP) {
                stop();

            } else if (event->getType() == MusicEvent::PAUSE) {
                pause();

            } else if (event->getType() == MusicEvent::SET_VOLUME) {
                auto volEvent = std::static_pointer_cast<SetVolumeEvent>(event);
                calculatedVolume = volEvent->getTo();

            } else if (event->getType() == MusicEvent::CHANGE_FILE) {
                auto fileEvent = std::static_pointer_cast<ChangeFileEvent>(event);
                load(fileEvent->getFilename());

            } else if (event->getType() == MusicEvent::SET_POSITION) {
                auto posEvent = std::static_pointer_cast<SetPositionEvent>(event);
                setPosition(posEvent->getPosition());

            } else if (event->getType() == MusicEvent::SET_LOOPING) {
                auto loopEvent = std::static_pointer_cast<SetLoopingEvent>(event);
                setLooping(loopEvent->isLooping());

            } else if (event->getType() == MusicEvent::SET_LOOP_POINTS) {
                auto loopEvent = std::static_pointer_cast<SetLoopPointsEvent>(event);
                setLoopPoints(loopEvent->getOffset(), loopEvent->getLength());

            } else if (event->getType() == MusicEvent::SET_LOOP_FADE) {
                auto loopEvent = std::static_pointer_cast<SetLoopFadeEvent>(event);
                setLoopFade(loopEvent->getFade());
            }

            events.erase(it);
            --it;
            
            if (verbose) {
                std::cout << "Finish event: " << event->toString() << "\n";
            }
        }
    }


    {
        double loopFactor = 0;
        if (fade == 0 || !music.getLoop()) {
            loopFactor = 1;

        } else {
            int currentMs = music.getPlayingOffset().asMilliseconds();
            auto loopPoints = music.getLoopPoints();
            int loopBeginMs = loopPoints.offset.asMilliseconds();
            int loopEndMs = loopBeginMs + loopPoints.length.asMilliseconds();

            // Don't use the loop fade-in on first loop
            if (firstLoop) {

                loopFactor = 1;
                if (currentMs - loopBeginMs > fade) {
                    firstLoop = false;
                }
                
            } else {
                loopFactor += std::max(0.0, std::min(1.0, ((double)(currentMs - loopBeginMs) / fade)));
            }
            
            loopFactor -= std::max(0.0, std::min(1.0, ((double)(currentMs - (loopEndMs - fade)) / fade)));
            loopFactor = std::max(0.0, std::min(1.0, loopFactor));
        }

        double finalVolume = calculatedVolume * masterVolume * loopFactor * 100;
        music.setVolume(finalVolume);
    }
}



void MusicManager::queueEvent(std::shared_ptr<MusicEvent> event) {
    if (verbose) {
        std::cout << "New event: " << event->toString() << "\n";
    }
    events.push_back(event);
}
