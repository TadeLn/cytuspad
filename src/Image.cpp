#include "Image.hpp"

#include <fstream>
#include <filesystem>



void Image::draw(Renderer& r) {
    for (int y = 0; y < 8; y++) {
        for (int x = 0; x < 8; x++) {
            r.setPixel(x + 1, 8 - y, pixels[y][x]);
        }
    }
}



std::optional<Image> Image::getFromFile(std::string filename) {
    auto it = loadedImages.find(filename);
    if (it != loadedImages.end()) {
        return it->second;
    }

    Image result;
    std::cout << "Loading image \"" << filename << "\"\n";

    std::ifstream file;
    file.open(filename, std::ios_base::in | std::ios_base::binary);
    if (file.is_open()) {
        char bytes[64];

        try {
            file.read(bytes, 64);
        } catch (std::exception&) {
            std::cerr << "[ERROR] Failed to load image \"" << filename << "\": File too short\n";
            loadedImages[filename] = std::optional<Image>();
            return std::optional<Image>();
        }

        file.close();

        result = Image((byte*)bytes);
    } else {
        std::cerr << "[ERROR] Failed to load image \"" << filename << "\": Could not open file\n";
        loadedImages[filename] = std::optional<Image>();
        return std::optional<Image>();
    }

    loadedImages[filename] = result;
    return std::optional(result);
}



Image::Image() {
    for (int y = 0; y < 8; y++) {
        std::array<byte, 8> arr;
        for (int x = 0; x < 8; x++) {
            arr[x] = 0;
        }
        this->pixels[y] = arr;
    }
}

Image::Image(std::array<std::array<byte, 8>, 8> pixels) {
    this->pixels = pixels;
}

Image::Image(byte pixels[64]) {
    for (int y = 0; y < 8; y++) {
        std::array<byte, 8> arr;
        for (int x = 0; x < 8; x++) {
            arr[x] = pixels[y * 8 + x];
        }
        this->pixels[y] = arr;
    }
}



void Image::reloadImages() {
    loadedImages.clear();
}



std::map<std::string, std::optional<Image>> Image::loadedImages;
