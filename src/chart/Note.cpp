#include "Note.hpp"


#include <cmath>

#include "tutils/json/Object.hpp"
#include "tutils/json/Array.hpp"
#include "tutils/json/Int.hpp"



int Note::getLane(double x) {
    return std::lround(x * 7);
}

int Note::getLane() {
    return getLane(this->x);
}


int Note::effectivePageIndex() {
    return this->pageIndex - ((int)this->isForward);
}



std::shared_ptr<tu::json::Node> Note::toJSON() const {
    auto root = std::make_shared<tu::json::Object>();
    // [TODO]
    return root;
}



bool Note::loadJSON(std::shared_ptr<tu::json::Node> json) {
    auto root = json->cast<tu::json::Object>();

    json->getProperty("page_index")->load(this->pageIndex);

    int typeInt = (int)type;
    json->getProperty("type")->load(typeInt);
    this->type = (NoteType)typeInt;

    json->getProperty("id")->load(this->id);
    json->getProperty("tick")->load(this->tick);
    json->getProperty("x")->load(this->x);
    json->getProperty("has_sibling")->load(this->hasSibling);
    json->getProperty("hold_tick")->load(this->holdTick);
    json->getProperty("next_id")->load(this->nextId);
    json->getProperty("is_forward")->load(this->isForward);
    
    return true;
}

Note Note::fromJSON(std::shared_ptr<tu::json::Node> json) {
    Note result;
    result.loadJSON(json);
    return result;
}
