#include "Chart.hpp"


#include <iostream>

#include "tutils/String.hpp"
#include "tutils/json/Object.hpp"
#include "tutils/json/Array.hpp"
#include "tutils/json/Int.hpp"

#include "Page.hpp"
#include "Tempo.hpp"
#include "EventOrder.hpp"
#include "Note.hpp"



int Chart::getTempoChangeFromTick(long tick) {
    for (int i = 0; i < tempoList.size(); i++) {
        Tempo change = tempoList[i];
        if (tick < change.tick) {
            return (i - 1 < 0) ? 0 : (i - 1);
        }
    }
    return tempoList.size() - 1;
}

double Chart::getTempoChangeMs(int i, double globalOffset) {
    if (i <= 0) {
        return globalOffset;
    } else {
        long tempoValue = tempoList[i - 1].value;
        long previousTick = tempoList[i - 1].tick;
        long currentTick = tempoList[i].tick;

        long ticks = currentTick - previousTick;

        return getTempoChangeMs(i - 1, globalOffset) + (tickToBeat(ticks) * tempoValue / 1000);
    }
}



double Chart::tickToBeat(long tick) {
    return tick / this->timeBase;
}

double Chart::beatToMs(double beat, double globalOffset) {
    int currentTempoChangeIndex = getTempoChangeFromTick(beatToTick(beat));
    long offset = getTempoChangeMs(currentTempoChangeIndex, globalOffset);

    Tempo currentTempoChange = this->tempoList[currentTempoChangeIndex];
    long startTick = currentTempoChange.tick;
    long tempoValue = currentTempoChange.value;
    double beats = beat - tickToBeat(startTick);

    return (beats * tempoValue / 1000) + offset;
}

long Chart::beatToMsInt(double beat, double globalOffset) {
    return (long)beatToMs(beat, globalOffset);
}

double Chart::tickToMs(long tick, double globalOffset) {
    return beatToMs(tickToBeat(tick), globalOffset);
}

long Chart::tickToMsInt(long tick, double globalOffset) {
    return (long)tickToMs(tick, globalOffset);
}



long Chart::beatToTick(double beat) {
    return beat * timeBase;
}

double Chart::msToBeat(double ms, double globalOffset) {
    double offset = globalOffset;
    double newOffset = offset;
    Tempo previousChange = this->tempoList[0];

    for (int i = 1; i < this->tempoList.size(); i++) {
        Tempo tempoChange = this->tempoList[i];
        double ticks = (tempoChange.tick - previousChange.tick);
        double beats = tickToBeat(ticks);

        newOffset += beats * (previousChange.value / 1000);

        if (newOffset >= ms) {
            break;
        }
        offset = newOffset;
        previousChange = tempoChange;
    }

    return ((ms - offset) * 1000 / previousChange.value) + tickToBeat(previousChange.tick);
}

long Chart::msToTick(double ms, double globalOffset) {
    return beatToTick(msToBeat(ms, globalOffset));
}



int Chart::getPageFromTick(long tick) {
    for (int i = 0; i < this->pageList.size(); i++) {
        Page page = this->pageList[i];
        if (tick >= page.startTick && tick < page.endTick) {
            return i;
        }
    }
    return -1;
}

double Chart::getYPiano(long tick, long currentTick) {
    // How much can you see at once (in ticks)
    const long visibility = beatToTick(2);
    return (double)(tick - currentTick) / visibility;
}

double Chart::getY(long tick, long currentTick, int pageIndex) {
    if (isPlayingPiano(tick)) {
        return getYPiano(tick, currentTick);
    }

    if (pageIndex == -1) pageIndex = getPageFromTick(tick);
    
    if (pageIndex < 0 || pageIndex >= this->pageList.size()) return 0;
    Page page = this->pageList[pageIndex];

    double progress = ((double)(tick - page.startTick)) / (page.endTick - page.startTick);
    Page::ScanLineDirection direction = page.scanLineDirection;
    if (direction == Page::ScanLineDirection::DOWN) {
        progress = 1.0 - progress;
    }

    return progress;
}

Page::ScanLineDirection Chart::getScanlineDirection(long tick, int pageIndex) {
    if (isPlayingPiano(tick)) {
        return Page::ScanLineDirection::DOWN;
    }

    if (pageIndex == -1) pageIndex = getPageFromTick(tick);
    
    if (pageIndex < 0 || pageIndex >= this->pageList.size()) {
        return Page::ScanLineDirection::DOWN;
    }
    Page page = this->pageList[pageIndex];

    return page.scanLineDirection;
}


long Chart::getPadY(double y, Page::ScanLineDirection direction) {
    int progress;
    if (direction == Page::ScanLineDirection::UP) {
        progress = (int)std::floor(y * 8);
    } else {
        progress = ((int)std::ceil(y * 8)) - 1;
    }
    return 1 + progress;
}


long Chart::getNoteX(Note& note) {
    return note.getLane() + 1;
}

long Chart::getNoteY(Note& note, long currentTick) {
    return this->getPadY(
        this->getY(note.tick, currentTick, note.pageIndex),
        this->getScanlineDirection(note.tick, note.pageIndex)
    );
}



// [TODO] optimize, use a variable, don't calculate every time
bool Chart::isPlayingPiano(long tick) {
    bool playing = false;

    for (int i = 0; i < this->eventOrderList.size(); i++) {
        EventOrder events = this->eventOrderList[i];

        if (events.tick >= tick) break;

        for (int j = 0; j < events.eventList.size(); j++) {
            Event event = events.eventList[j];
            if (event.type == Event::START_PIANO_SEGMENT) {
                playing = false;
            } else if (event.type == Event::END_PIANO_SEGMENT) {
                playing = true;
            }
        }
    }
    return playing;
}



std::string Chart::getDiffName(std::string diff, bool showIndex) {
    try {
        int diffInt = std::stoi(diff);
        return getDiffName(diffInt, showIndex);
    } catch (std::exception& e) {
        return tu::String::str("{", diff, "} CUSTOM");
    }
}

std::string Chart::getDiffName(int diff, bool showIndex) {
    const std::array<std::string, 6> difficultyNames {
        "EASY",
        "HARD",
        "CHAOS",
        "GLITCH",
        "CRASH",
        "DREAM"
    };

    if (diff < 0 || diff >= difficultyNames.size()) {
        return tu::String::str("{", diff, "} CUSTOM");
    } else {
        return showIndex
            ? tu::String::str("{", diff, "} ", difficultyNames[diff])
            : difficultyNames[diff];
    }
}



std::shared_ptr<tu::json::Node> Chart::toJSON() const {
    auto root = std::make_shared<tu::json::Object>();
    // [TODO]
    return root;
}



bool Chart::loadJSON(std::shared_ptr<tu::json::Node> json) {
    auto root = json->cast<tu::json::Object>();

    json->getProperty("format_version")->load(this->formatVersion);
    json->getProperty("time_base")->load(this->timeBase);
    json->getProperty("start_offset_time")->load(this->startOffsetTime);

    for (auto x : json->getProperty("page_list")->cast<tu::json::Array>()->getOr<tu::json::Node::array>()) {
        this->pageList.push_back(Page::fromJSON(x));
    }
    for (auto x : json->getProperty("tempo_list")->cast<tu::json::Array>()->getOr<tu::json::Node::array>()) {
        this->tempoList.push_back(Tempo::fromJSON(x));
    }
    for (auto x : json->getProperty("event_order_list")->cast<tu::json::Array>()->getOr<tu::json::Node::array>()) {
        this->eventOrderList.push_back(EventOrder::fromJSON(x));
    }
    for (auto x : json->getProperty("note_list")->cast<tu::json::Array>()->getOr<tu::json::Node::array>()) {
        this->noteList.push_back(Note::fromJSON(x));
    }
    
    return true;
}

Chart Chart::fromJSON(std::shared_ptr<tu::json::Node> json) {
    Chart result;
    result.loadJSON(json);
    return result;
}

Chart Chart::fromFile(std::string filename) {
    if (filename == "") {
        return Chart();
    } else {
        return Chart::fromJSON(tu::json::Node::fromFile(filename));
    }
}
