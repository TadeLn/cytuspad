#pragma once


#include <string>

#include "tutils/json/Serializable.hpp"
#include "tutils/json/Deserializable.hpp"



class Event : public tu::json::Serializable, public tu::json::Deserializable {


public:
    enum EventType {
        START_PIANO_SEGMENT = 6,
        END_PIANO_SEGMENT = 7,
    };

    EventType type;
    std::string args;


    std::shared_ptr<tu::json::Node> toJSON() const;

    bool loadJSON(std::shared_ptr<tu::json::Node> json);
    static Event fromJSON(std::shared_ptr<tu::json::Node> json);


};
