#pragma once


#include "tutils/json/Serializable.hpp"
#include "tutils/json/Deserializable.hpp"



class Note : public tu::json::Serializable, public tu::json::Deserializable {


public:
    enum NoteType {
        HIT,
        HOLD,
        HOLD_LONG, // yellow pillar
        DRAG,
        DRAG_TICK,
        SWIPE,
        HIT_DRAG,
        HIT_DRAG_TICK,
        PIANO_HIT,
        PIANO_DRAG
    };

    int pageIndex = 0;
    NoteType type = HIT;
    int id = 0;
    long tick = 0;
    double x = 0.5; // from 0 (left) to 1 (right); 0.5 = middle
    bool hasSibling = false;
    int holdTick = 0;
    int nextId = 0;
    bool isForward = false;


    static int getLane(double x); // Returns a number from 0 to 7
    int getLane(); // Returns a number from 0 to 7

    int effectivePageIndex();


    std::shared_ptr<tu::json::Node> toJSON() const;

    bool loadJSON(std::shared_ptr<tu::json::Node> json);
    static Note fromJSON(std::shared_ptr<tu::json::Node> json);


};
