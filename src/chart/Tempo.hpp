#pragma once


#include "tutils/json/Serializable.hpp"
#include "tutils/json/Deserializable.hpp"



class Tempo : public tu::json::Serializable, public tu::json::Deserializable {


public:
    // Tempo change position
    int tick = 0;

    // New tempo (in microseconds per beat)
    int value = 500000;


    double tempoValueToBpm(long value);
    long bpmToTempoValue(double bpm);


    std::shared_ptr<tu::json::Node> toJSON() const;

    bool loadJSON(std::shared_ptr<tu::json::Node> json);
    static Tempo fromJSON(std::shared_ptr<tu::json::Node> json);


};
