#pragma once


#include <vector>

#include "tutils/json/Serializable.hpp"
#include "tutils/json/Deserializable.hpp"

#include "Page.hpp"
#include "Tempo.hpp"
#include "EventOrder.hpp"
#include "Note.hpp"



class Chart : public tu::json::Serializable, public tu::json::Deserializable {


public:
    int formatVersion = 0;

    // Amount of ticks per beat
    int timeBase = 480;

    double startOffsetTime = 0.0;

    std::vector<Page> pageList;
    std::vector<Tempo> tempoList;
    std::vector<EventOrder> eventOrderList;
    std::vector<Note> noteList;


    // Returns the index of the tempo change, not the tempo itself
    int getTempoChangeFromTick(long tick);

    double getTempoChangeMs(int i, double globalOffset);

    double tickToBeat(long tick);
    double beatToMs(double beat, double globalOffset);
    long beatToMsInt(double beat, double globalOffset);
    double tickToMs(long tick, double globalOffset);
    long tickToMsInt(long tick, double globalOffset);

    long beatToTick(double beat);
    double msToBeat(double ms, double globalOffset);
    long msToTick(double ms, double globalOffset);

    // Returns the index of the page, not the page itself
    int getPageFromTick(long tick);

    // Returns a value from 0 to 1
    double getYPiano(long tick, long currentTick);

    // Returns a value from 0 to 1
    double getY(long tick, long currentTick, int pageIndex = -1);

    // Returns a ScanLineDirection
    Page::ScanLineDirection getScanlineDirection(long tick, int pageIndex = -1);


    long getPadY(double y, Page::ScanLineDirection direction);

    static long getNoteX(Note& note);
    long getNoteY(Note& note, long currentTick);


    bool isPlayingPiano(long tick);


    static std::string getDiffName(std::string diff, bool showIndex = false);
    static std::string getDiffName(int diff, bool showIndex = false);


    std::shared_ptr<tu::json::Node> toJSON() const;

    bool loadJSON(std::shared_ptr<tu::json::Node> json);
    static Chart fromJSON(std::shared_ptr<tu::json::Node> json);
    static Chart fromFile(std::string filename);


};
