#include "Page.hpp"


#include "tutils/json/Object.hpp"



std::shared_ptr<tu::json::Node> Page::toJSON() const {
    auto root = std::make_shared<tu::json::Object>();
    // [TODO]
    return root;
}



bool Page::loadJSON(std::shared_ptr<tu::json::Node> json) {
    auto root = json->cast<tu::json::Object>();
    
    json->getProperty("start_tick")->load(this->startTick);
    json->getProperty("end_tick")->load(this->endTick);

    int scanLineDirectionInt = (int)scanLineDirection;
    json->getProperty("scan_line_direction")->load(scanLineDirectionInt);
    this->scanLineDirection = (ScanLineDirection)scanLineDirectionInt;
    
    return true;
}

Page Page::fromJSON(std::shared_ptr<tu::json::Node> json) {
    Page result;
    result.loadJSON(json);
    return result;
}
