#include "EventOrder.hpp"


#include "tutils/json/Object.hpp"
#include "tutils/json/Array.hpp"
#include "tutils/json/Int.hpp"

#include "Event.hpp"



std::shared_ptr<tu::json::Node> EventOrder::toJSON() const {
    auto root = std::make_shared<tu::json::Object>();
    // [TODO]
    return root;
}



bool EventOrder::loadJSON(std::shared_ptr<tu::json::Node> json) {
    auto root = json->cast<tu::json::Object>();

    json->getProperty("tick")->load(this->tick);

    for (auto x : json->getProperty("event_list")->cast<tu::json::Array>()->getOr<tu::json::Node::array>()) {
        this->eventList.push_back(Event::fromJSON(x));
    }
    
    return true;
}

EventOrder EventOrder::fromJSON(std::shared_ptr<tu::json::Node> json) {
    EventOrder result;
    result.loadJSON(json);
    return result;
}
