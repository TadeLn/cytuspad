#pragma once


#include <vector>

#include "tutils/json/Serializable.hpp"
#include "tutils/json/Deserializable.hpp"

#include "Event.hpp"



class EventOrder : public tu::json::Serializable, public tu::json::Deserializable {


public:
    int tick = 0;
    std::vector<Event> eventList;


    std::shared_ptr<tu::json::Node> toJSON() const;

    bool loadJSON(std::shared_ptr<tu::json::Node> json);
    static EventOrder fromJSON(std::shared_ptr<tu::json::Node> json);


};
