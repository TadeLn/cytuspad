#include "Tempo.hpp"


#include "tutils/json/Object.hpp"
#include "tutils/json/Array.hpp"
#include "tutils/json/Int.hpp"



double Tempo::tempoValueToBpm(long value) {
    return 60000000.0 / value;
}

long Tempo::bpmToTempoValue(double bpm) {
    return 60000000.0 / bpm;
}




std::shared_ptr<tu::json::Node> Tempo::toJSON() const {
    auto root = std::make_shared<tu::json::Object>();
    // [TODO]
    return root;
}



bool Tempo::loadJSON(std::shared_ptr<tu::json::Node> json) {
    auto root = json->cast<tu::json::Object>();

    json->getProperty("tick")->load(this->tick);
    json->getProperty("value")->load(this->value);
    
    return true;
}

Tempo Tempo::fromJSON(std::shared_ptr<tu::json::Node> json) {
    Tempo result;
    result.loadJSON(json);
    return result;
}
