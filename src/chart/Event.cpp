#include "Event.hpp"


#include "tutils/json/Object.hpp"
#include "tutils/json/Array.hpp"
#include "tutils/json/Int.hpp"



std::shared_ptr<tu::json::Node> Event::toJSON() const {
    auto root = std::make_shared<tu::json::Object>();
    // [TODO]
    return root;
}



bool Event::loadJSON(std::shared_ptr<tu::json::Node> json) {
    auto root = json->cast<tu::json::Object>();

    int typeInt = (int)type;
    json->getProperty("type")->load(typeInt);
    this->type = (EventType)typeInt;

    json->getProperty("args")->load(this->args);
    
    return true;
}

Event Event::fromJSON(std::shared_ptr<tu::json::Node> json) {
    Event result;
    result.loadJSON(json);
    return result;
}
