#pragma once


#include "tutils/json/Serializable.hpp"
#include "tutils/json/Deserializable.hpp"



class Page : public tu::json::Serializable, public tu::json::Deserializable {


public:
    enum ScanLineDirection {
        UP = 1,
        DOWN = -1
    };

    int startTick = 0;
    int endTick = 0;
    ScanLineDirection scanLineDirection = UP;


    std::shared_ptr<tu::json::Node> toJSON() const;

    bool loadJSON(std::shared_ptr<tu::json::Node> json);
    static Page fromJSON(std::shared_ptr<tu::json::Node> json);


};
