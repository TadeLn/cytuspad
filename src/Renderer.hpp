#pragma once


#include "Util.hpp"
#include "Pad.hpp"
#include "Color.hpp"

class Game;



class Renderer {


public:
    Game* g;
    int frameCounter = 0;

    byte getPixel(byte pitch);
    byte getPixel(unsigned int x, unsigned int y);
    void setPixel(byte pitch, byte color);
    void setPixel(unsigned int x, unsigned int y, byte color);

    void clear();
    void display();

    void drawHorizontalLine(int x, int y, byte color, int width = 10);
    void drawVerticalLine(int x, int y, byte color, int height = 10);
    void drawRect(int x, int y, int width, int height, byte color);

    Renderer(Game* g);


};
