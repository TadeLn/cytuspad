#include "Game.hpp"

#include <thread>

#include "Renderer.hpp"
#include "screen/Screen.hpp"
#include "screen/GameScreen.hpp"
#include "screen/ColorTestScreen.hpp"
#include "screen/RainbowScreen.hpp"
#include "screen/SongSelectScreen.hpp"



void Game::reloadSettings() {
    Settings newSettings;
    if (!newSettings.loadJSON(tu::json::Node::fromFile("settings.json"))) {
        std::cerr << "Error while loading settings.json. Leaving old settings.\n";
    } else {
        std::cout << "Reloaded settings.\n";
        settings = newSettings;

        in.setChannel(settings.midiInputChannel);
        out.setChannel(settings.midiOutputChannel);
    }
}



void Game::scanSongs(std::filesystem::path path) {
    if (!std::filesystem::exists(path)) return;
    if (!std::filesystem::is_directory(path)) return;

    auto infoJsonPath = path / "info.json";
    if (std::filesystem::exists(infoJsonPath)) {
        if (std::filesystem::is_regular_file(infoJsonPath)) {
            SongEntry entry = SongEntry::fromFile(path);
            songs[path] = entry;
            std::cout << std::left << std::setw(30) << (std::string("[Scanning] Found song #") + std::to_string(songs.size()) + ": ") << entry << "\n";
            return;
        }
    }

    for (const auto& file : std::filesystem::directory_iterator(path)) {
        if (file.is_directory()) {
            scanSongs(path / file.path().filename());
        }
    }
}

void Game::scanSongs() {
    songs.clear();
    auto cwd = std::filesystem::current_path();
    std::filesystem::current_path("res/charts");
    if (!std::filesystem::exists(".")) return;
    if (!std::filesystem::is_directory(".")) return;
    for (const auto& file : std::filesystem::directory_iterator(".")) {
        if (file.is_directory()) {
            scanSongs(std::filesystem::path() / file.path().filename());
        }
    }
    std::filesystem::current_path(cwd);

    if (songs.size() == 0) {
        throw tu::Exception("Error: No songs found. I'm outta here", EXCTX);
    }

    // Sort songs
    sortedSongs.clear();
    for (auto x : songs) {
        sortedSongs.push_back(std::make_pair(x.second.playlistOrder, x.first));
    }

    std::sort(sortedSongs.begin(), sortedSongs.end(), [](std::pair<int, std::string> a, std::pair<int, std::string> b){
        if (a.first != b.first) {
            // -1 goes to the end of the list
            if (a.first == -1) return false;
            if (b.first == -1) return true;
            
            return a.first < b.first;
        } else {
            return a.second < b.second;
        }
    });
}

void Game::printSongs() {
    std::cout << "Currently loaded songs: \n";

    for (int i = 0; i < sortedSongs.size(); i++) {
        auto song = songs[sortedSongs[i].second];
        std::cout << std::right << std::setw(6) << (std::to_string(i + 1) + ". ") << song << "\n";
    }
}



void Game::openScreen(Screen* screen, bool clear) {
    Screen* oldScreen = this->currentScreen;
    screen->parentScreen = oldScreen;
    screen->open(this);

    this->currentScreen = screen;

    if (clear) {
        r->clear();
    }
}

void Game::closeScreen(bool clear) {
    Screen* screen = this->currentScreen;
    this->currentScreen = screen->parentScreen;
    if (screen != nullptr) {
        screen->close();
        delete screen;
    }

    if (currentScreen == nullptr) {
        exit();
    } else {
        currentScreen->reopen();
    }

    if (clear) {
        r->clear();
    }
}

void Game::replaceScreen(Screen* screen, bool clear) {
    Screen* oldScreen = this->currentScreen;
    if (oldScreen != nullptr) {
        oldScreen->close();
        delete oldScreen;
    }

    screen->parentScreen = oldScreen->parentScreen;
    screen->open(this);

    this->currentScreen = screen;

    if (clear) {
        r->clear();
    }
}



void Game::start() {
    isRunning = true;

    if (!settings.loadJSON(tu::json::Node::fromFile("settings.json"))) {
        throw tu::Exception("Critical error while loading settings.json. Aborting.", EXCTX);
    }

    {
        RtMidiOut* midiOut = Util::getOutputMIDIConnection();
        if (midiOut == nullptr) throw tu::Exception("MidiOut could not be created", EXCTX);
        out.setMIDIDevice(midiOut);
    }
    {
        RtMidiIn* midiIn = Util::getInputMIDIConnection([](double timestamp, std::vector<unsigned char>* message, void* userData){
            Game::inst()->in.handleMIDIMessage(timestamp, *message);
        });
        if (midiIn == nullptr) throw tu::Exception("MidiIn could not be created", EXCTX);
        in.setMIDIDevice(midiIn);
    }


    scanSongs();

    music.init();
    sfx.init();



    openScreen(new SongSelectScreen());

    out.forceClear();
    out.display();

    while (isRunning) {
        loop();
    }
}

void Game::loop() {
    ControllerInput::MidiMessage msg;
    while (in.pollMessage(msg)) {
        if (settings.verboseInput) {
            std::cout << "Recieved message @ " << msg.timestamp << " \t| ";
            for (int i = 0; i < msg.data.size(); i++) {
                std::cout << (unsigned int)msg.data.at(i) << " ";
            }
        }

        byte messageType = msg.data.at(0);
        byte messageChannel = messageType & 0b00001111;
        byte messageTypeOnly = messageType & 0b11110000;

        if (settings.verboseInput) {
            std::cout << " \t| Channel: " << (unsigned int)messageChannel << " Type: " << (unsigned int)messageTypeOnly << "\n";
        }

        if (messageChannel == in.getChannel()) {
            
            if (messageTypeOnly == 144 || messageTypeOnly == 176) {
                byte pitch = msg.data.at(1);
                byte velocity = msg.data.at(2);
                currentScreen->onInput(pitch, velocity);
            }
        }
    }

    music.update();

    currentScreen->loop();
    currentScreen->draw(*r);
    std::this_thread::sleep_for(std::chrono::milliseconds(5));
}

void Game::exit() {
    isRunning = false;
}



Game::Game() {
    Game::i = this;
    this->r = new Renderer(this);
}

Game::~Game() {
    in.destroyMIDIDevice();
    out.forceClear();
    out.display();
    out.destroyMIDIDevice();
    delete r;
}

Game* Game::inst() {
    return Game::i;
}

Game* Game::i;
