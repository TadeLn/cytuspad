#pragma once


#include <vector>

#include "tutils/json/Serializable.hpp"
#include "tutils/json/Deserializable.hpp"



class Settings : public tu::json::Serializable, public tu::json::Deserializable {


public:
    std::string tapfxFilename = "";
    bool verboseInput = false;
    int midiInputChannel = 0;
    int midiOutputChannel = 0;


    std::shared_ptr<tu::json::Node> toJSON() const;

    bool loadJSON(std::shared_ptr<tu::json::Node> json);
    static Settings fromJSON(std::shared_ptr<tu::json::Node> json);


};
