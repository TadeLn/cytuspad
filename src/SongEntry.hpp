#pragma once


#include "tutils/json/Serializable.hpp"
#include "tutils/json/Deserializable.hpp"



class SongEntry : public tu::json::Serializable, public tu::json::Deserializable {


public:
    std::string filename = "";
    std::string title = "(unknown)";
    std::string artist = "(unknown)";
    std::string charter = "(unknown)";
    int previewOffset = 30000;
    int previewLength = 30000;
    int playlistOrder = -1;
    int getDifficulty(unsigned int index);
    int getDifficultyCount();

    std::shared_ptr<tu::json::Node> toJSON() const;

    bool loadJSON(std::shared_ptr<tu::json::Node> json);
    static SongEntry fromJSON(std::shared_ptr<tu::json::Node> json);
    static SongEntry fromFile(std::string filename);

    std::string toString() const;
    std::string toString1() const;
    std::string toString2() const;
    std::string toString3() const;
    friend std::ostream& operator<<(std::ostream& stream, const SongEntry& instance);

    SongEntry(std::string filename, std::string title, std::string artist, int previewOffset, int previewLength);
    SongEntry() = default;

private:
    std::vector<int> difficulties;
    void setDifficulty(unsigned int index, int value);


};
